#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_sleep.h"
#include "driver/rtc_io.h"

#define BOTAO 0 

RTC_DATA_ATTR int Acordou = 0;

void app_main(void)
{
  // 1. Utiliza o Sleep Timer (em microsegundos)
  esp_sleep_enable_timer_wakeup(3 * 1000000);

  // 2. Utiliza a GPIO para acordar a ESP
  rtc_gpio_pullup_en(BOTAO);
  rtc_gpio_pulldown_dis(BOTAO);
  esp_sleep_enable_ext0_wakeup(BOTAO, 0);

  printf("Acordou %d vezes \n", Acordou++);
  printf("Entrando em modo Deep Sleep\n");

  // Coloca a ESP no Deep Sleep
  esp_deep_sleep_start();
  
}